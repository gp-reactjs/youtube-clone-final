import React, {Component} from 'react';


  const VideoListItem = (props) => {

    const video = props.videos;
    const selectVideo = props.onVideoClick;
    const imgUrl = video.snippet.thumbnails.default.url;

    return(
       <li onClick={() => selectVideo(video)} className="list-group-item pointr">
          <div className="video-list media">
            <div className="media-left">
              <img className="media-object" src={imgUrl} />
            </div>

            <div className="media-body">
              <div className="media-heading">{video.snippet.title}</div>
            </div>
          </div>
       </li>
    );
  }

export default VideoListItem;
