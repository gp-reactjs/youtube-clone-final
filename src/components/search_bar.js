import React, { Component } from 'react';

// React Classes
class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = { term: '' };
  }
  render() {
    return (
      <div className="search-bar">
        <input
          value={this.state.term}
          onChange={event => this.onInputChange(event.target.value)}
        />
      </div>
    );
  }

  // This will handle change of input
  onInputChange(term) {
    this.setState({term});
    this.props.onSearchTermChange(term);
  }


}

export default SearchBar;
