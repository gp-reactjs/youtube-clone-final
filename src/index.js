import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import YTSearch from 'youtube-api-search';
import _ from 'lodash';

// Search Bar Component
import SearchBar from './components/search_bar';
import VideoList from './components/video_list';
import VideoDetails from './components/video_details';


// Youtube api key
const API_KEY = "AIzaSyAcq8xnNAkqZ1Sye398AE5Ccr3EP8GTjaA";

// Create new component and it will produce
// some html
class App extends Component {
  constructor(props) {
      super(props);

      this.state = {
        videos: [],
        selectedVideo: null
      };

      // Call Video API
      this.videoSearch('swift 3');
  }

  videoSearch(term) {
    // Getting data from youtube
    YTSearch({key: API_KEY, term: term}, (videos) => {
      this.setState({
        videos:  videos,
        selectedVideo: videos[0]
      });
    });
  }

  render() {

    const videoSearch = _.debounce((term) => {this.videoSearch(term)}, 500);

    return (
      <div>
        <SearchBar onSearchTermChange={videoSearch}/>
        <VideoDetails video={this.state.selectedVideo} />
        <VideoList onVideoSelect={selectedVideo => this.setState({selectedVideo})} video={this.state.videos} />
      </div>
    );
  }
}


// Take this component's generated HTML and put it
// on the page (in the DOM)
ReactDOM.render(<App />, document.querySelector(".container"));
